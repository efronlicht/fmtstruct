// Package fmtstruct writes slices of structs as formatted output in JSON, CSV, or tabular format.
//
// # Usage (library):
//   - Set a [Mode] to CSV, JSON, JSON_PRETTY, or TABLE to determine the output format,
//   - Use [Print] (mode, row...) to print to stdout using default configuration,
//   - Use [WriteTo](w, mode, opts, row...) to write to an io.Writer with custom options.
//
// # Usage (CLI):
//   - Use [RegisterCLI](flag.CommandLine, defaultMode) to register command-line flags for the output mode and options.
//
// Everything else is just configuration, flag parsing, and convenience functions.
// # Anticipated questions:
//
//   - Why only structs?
//
//     A: that's what I needed.
package fmtstruct

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"slices"
	"strings"
	"text/tabwriter"
	"unsafe"
)

type (
	// Mode specifies the output format for WriteOutput.
	// Valid modes are:
	//   - JSON: compact JSON output with no indentation
	//   - JSON_PRETTY: JSON output with indentation
	//   - TABLE: tabular output via text/tabwriter
	//   - CSV: CSV output
	// It implements flag.Value and encoding.Marshaler/Unmarshaler, so it can be
	// easily obtained via environment variables, flags, or configuration files.
	// The integer values are subject to change, so don't rely on them.
	Mode byte

	// Formatting options for WriteTable and WriteCSV. The nil value is OK to use and will be treated as
	// DefaultCSVOptions, DefaultTableOptions, etc as appropriate.
	// Use (*Opts).AddFlagSet register command-line flags for these options.
	Opts struct {
		AllowOmitEmpty bool                                      // allow omitting empty fields with the 'omitempty' tag
		FormatVerbs    map[string]string                         // map of field names to format verbs. If empty, fmt.Sprintf("%v") is used. Exactly one of Verb or CustomFormat should be set for a field.
		Fields         []string                                  // Which fields to print. If empty, all (exported) fields are printed.
		NoHeader       bool                                      // Do not print the header row.
		PrintSeparator bool                                      // Print a separator line between the header and the row. Default: ON for PrintTable, otherwise OFF.
		CustomFormat   map[string]func(w io.Writer, v any) error // Custom formatters for specific fields. Exactly one of Verb or CustomFormat should be set for a field.
	}
)

// Write output in a format determined by the Mode and (optionally) Opts to the io.Writer `w`.
//
// `T` must be a struct type.
// If opts are nil, the Mode.DefaultOptions() are used instead.
func WriteTo[T any](w io.Writer, mode Mode, opts *Opts, row ...T) error {
	if opts == nil {
		opts = mode.DefaultOptions()
	}
	switch mode {
	case JSON:
		return json.NewEncoder(w).Encode(row)
	case JSON_PRETTY:
		enc := json.NewEncoder(w)
		enc.SetIndent("", "  ")
		return enc.Encode(row)
	case TABLE:
		return WriteTable(w, opts, row...)
	case CSV:
		return WriteCSV(w, opts, row...)
	default:
		return fmt.Errorf("unknown mode: %v", mode)
	}
}

// Print a slice of structs to stdout using the given [Mode] and it's [Mode.DefaultOptions].
// This is a convenience wrapper around WriteTable(os.Stdout, nil, row...)
// T must be a struct type.
func Print[T any](mode Mode, row ...T) error {
	return WriteTo(os.Stdout, mode, nil, row...)
}

var modeNames = [FORMAT_MODE_N]string{
	MODE_UNSET:  "<unset>",
	JSON:        "json",
	JSON_PRETTY: "json-pretty",
	TABLE:       "table",
	CSV:         "csv",
}

func (fm *Mode) UnmarshalText(text []byte) error {
	mode, err := ParseMode(string(text))
	if err == nil {
		*fm = mode
	}
	return err
}
func (fm *Mode) MarshalText() ([]byte, error) { return []byte(fm.String()), nil }

// DefaultOptions returns the default options for the given mode. Don't modify the returned value; make a copy if you need to.
func (fm Mode) DefaultOptions() *Opts { return &defaultOptions[fm] }

// String returns the name of the mode: "json", "json-pretty", "table", or "csv".
func (fm Mode) String() string {
	if int(fm) >= len(modeNames) {
		return fmt.Sprintf("<unknown mode %d>", fm)
	}
	return modeNames[fm]
}

var defaultOptions = [FORMAT_MODE_N]Opts{
	JSON:        {AllowOmitEmpty: true},
	JSON_PRETTY: {AllowOmitEmpty: true},
	TABLE:       {AllowOmitEmpty: true, PrintSeparator: true},
	CSV:         {AllowOmitEmpty: true},
}

// ParseMode parses a string into a Mode. It returns an error if the string is not a valid mode.
func ParseMode(s string) (Mode, error) {
	for i := Mode(0); i < FORMAT_MODE_N; i++ {
		if modeNames[i] == s {
			return Mode(i), nil
		}
	}
	return 0, fmt.Errorf("unknown mode %q: expected one of %s", s, modeNames[1:])
}

const (
	MODE_UNSET    Mode = iota // not yet set. presence indicates programmer error
	JSON                      // JSON output with no indentation
	JSON_PRETTY               // JSON output with indentation
	TABLE                     // tabular output via text/tabwriter
	CSV                       // CSV output
	FORMAT_MODE_N             // number of modes: length placeholder
)

// PrintOutput prints the row to stdout in the given mode, using the default options for that mode.
// This is syntactic sugar for WriteOutput(os.Stdout, mode, nil, row...)
func PrintOutput[T any](mode Mode, row ...T) error {
	return WriteTo(os.Stdout, mode, nil, row...)
}

func (o Opts) validate() error {
	if o.NoHeader && o.PrintSeparator {
		return fmt.Errorf("NoHeader and PrintSeparator are mutually exclusive")
	}
	for k, v := range o.FormatVerbs {
		if strings.Count(v, "%") != 1 {
			return fmt.Errorf("invalid format verb %q for field %q: expected exactly one %%", v, k)
		}
		if o.CustomFormat[k] != nil {
			return fmt.Errorf("field %q has both a format verb and a custom formatter", k)
		}
	}
	for k := range o.CustomFormat {
		if o.FormatVerbs[k] != "" {
			return fmt.Errorf("field %q has both a format verb and a custom formatter", k)
		}
	}
	for k, v := range o.CustomFormat {
		if v == nil {
			return fmt.Errorf("nil custom formatter for field %q", k)
		}
	}
	for _, f := range o.Fields {
		if f == "" {
			return fmt.Errorf("empty field name in Fields")
		}
	}

	return nil
}

type fmtfn = func(w io.Writer, v any) error

// fmtVerb calls fmt.Fprintf(w, verb, v)
func fmtVerb(verb string, omitEmpty bool) fmtfn {
	return func(w io.Writer, v any) error {
		if omitEmpty && reflect.ValueOf(v).IsZero() {
			return nil
		}
		_, err := fmt.Fprintf(w, verb, v)
		return err
	}
}

// fmtJSON writes v as JSON to w.
func formatFuncs(t reflect.Type, idx []int, formatVerbs map[string]string, customFormat map[string]fmtfn, omitEmpty []bool) []fmtfn {
	fns := make([]fmtfn, len(idx))
	for j, i := range idx {
		j, i := j, i
		f := t.Field(i)
		if fn := customFormat[f.Name]; fn != nil {
			fns[j] = fn
			continue
		}

		verb := formatVerbs[f.Name]
		if verb == "" {
			verb = "%v"
		}
		fns[j] = fmtVerb(verb, omitEmpty[i])
	}
	return fns
}

func headerColOmitEmpty(t reflect.Type, fields []string) (header []string, col []int, omitempty []bool) {
	cap := len(fields)
	if cap == 0 {
		cap = t.NumField()
	}
	header, col = make([]string, 0, cap), make([]int, 0, cap)

	omitempty = make([]bool, t.NumField())
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if !f.IsExported() {
			continue
		}

		if len(fields) > 0 && !slices.Contains(fields, f.Name) {
			continue
		}
		var after string
		before, after, _ := strings.Cut(f.Tag.Get("json"), ",")
		if before == "" {
			header = append(header, f.Name)
		} else {
			header = append(header, before)
		}
		col = append(col, i)
		omitempty[i] = after == "omitempty"

	}
	return header, col, omitempty
}

func writeHeaderSep(writerow func([]string) error, header []string, writeHeader, writeSeparator bool) error {
	switch {
	case writeHeader && writeSeparator:
		if err := writerow(header); err != nil {
			return err
		}
		for i := range header {
			header[i] = "----"
		}
		return writerow(header)
	case !writeHeader && writeSeparator:
		return fmt.Errorf("writeHeader and writeSeparator are mutually exclusive")
	case !writeHeader && !writeSeparator:
		return nil
	case writeHeader && !writeSeparator:
		return writerow(header)
	default:
		panic("unreachable")
	}
}

// WriteCSV writes a slice of structs to a CSV file, using the JSON tags as the column headers.
// See opts for more options.
func WriteCSV[T any](w io.Writer, opt *Opts, row ...T) error {
	if opt == nil {
		opt = &Opts{}
	}
	if err := opt.validate(); err != nil {
		return fmt.Errorf("validating %T: %w", opt, err)
	}
	var derefCount int
	t := reflect.TypeFor[T]()
	for t.Kind() == reflect.Ptr {
		derefCount++
		t = t.Elem()
		if derefCount > 5 {
			return fmt.Errorf("fmtstruct: too many pointer dereferences")
		}
	}

	if t.Kind() != reflect.Struct {
		return fmt.Errorf("fmtstruct: expected a slice of structs, got %v", t)
	}
	header, col, omitempty := headerColOmitEmpty(t, opt.Fields)
	cw := csv.NewWriter(w)
	defer cw.Flush()
	if len(opt.Fields) > 0 && len(opt.Fields) != len(header) {
		return fmt.Errorf("struct %T has only %d of the %d requested fields: %v", t, len(header), len(opt.Fields), opt.Fields)
	}
	writeRow := func(a []string) error { return cw.Write(a) }
	if err := writeHeaderSep(writeRow, header, !opt.NoHeader, opt.PrintSeparator); err != nil {
		return err
	}

	fns := formatFuncs(t, col, opt.FormatVerbs, opt.CustomFormat, omitempty)

	var buf bytes.Buffer
	formattedOff := make([]int, len(col))
	formatted := make([]string, len(col))
	for i := range row {
		v := reflect.ValueOf(row[i])
		for i := 0; i < derefCount; i++ {
			v = v.Elem()
		}
		buf.Reset()
		for j, offset := range col {
			formattedOff[j] = buf.Len()
			f := v.Field(offset)
			if omitempty[offset] && f.IsZero() {
				continue // skip empty fields entirely
			}
			if err := fns[offset](&buf, f.Interface()); err != nil { // format the field into the buffer
				return err
			}
		}

		// get a temporary reference to the buffer's contents as a string, WITHOUT copying.
		// this is very dangerous! luckily, we know we will not modify the buffer until after we are
		// done writing to the CSV's internal buffer, so we can get away with it.
		s := unsafe.String(unsafe.SliceData(buf.Bytes()), buf.Len())
		for j := range col {
			start, end := formattedOff[j], len(s)
			if j < len(col)-1 {
				end = formattedOff[j+1]
			}
			formatted[j] = s[start:end]
		}

		if err := cw.Write(formatted); err != nil {
			return err
		}
	}
	return nil
}

// Print a slice of structs to stdout using the JSON tags as the column headers.
// This is a convenience wrapper around WriteTable(os.Stdout, TABLE.DefaultOptions(), row...)
// The row slice must be a slice of structs.
func PrintTable[T any](row ...T) error {
	return WriteTable(os.Stdout, TABLE.DefaultOptions(), row...)
}

// Write a table to the given io.Writer. The table is a slice of structs, where
// the JSON tags are used as the column headers (if they exist) and the struct names otherwise.
// Yes, this is overloading the meaning of the 'JSON' struct tags, but we just want consistency.
func WriteTable[T any](dst io.Writer, opts *Opts, row ...T) error {
	if err := opts.validate(); err != nil {
		return fmt.Errorf("validating %T: %w", opts, err)
	}
	if opts == nil {
		opts = TABLE.DefaultOptions()
	}
	const minwidth, tabwidth, padding, padchar, flags = 4, 4, 2, ' ', 0
	tw := tabwriter.NewWriter(dst, minwidth, tabwidth, padding, padchar, flags)
	defer tw.Flush()
	t := reflect.TypeFor[T]()
	if t.Kind() != reflect.Struct {
		return fmt.Errorf("fmtstruct: expected a slice of structs, got %v", t)
	}

	// find out which fields to print
	header, col, omitempty := headerColOmitEmpty(t, opts.Fields)
	writeRow := func(s []string) error {
		_, err := fmt.Fprintln(tw, strings.Join(s, "\t"))
		return err
	}
	writeHeaderSep(writeRow, header, !opts.NoHeader, opts.PrintSeparator)
	// now it should look like
	// COL0 COL1 COL2
	// ---- ---- ----

	// write the row
	fn := formatFuncs(t, col, opts.FormatVerbs, opts.CustomFormat, omitempty)
	for i := range row {
		v := reflect.ValueOf(row[i])
		for j, offset := range col {
			f := v.Field(offset)
			fn[j](tw, f.Interface())

			if j < len(col)-1 {
				tw.Write([]byte("\t"))
			} else {
				tw.Write([]byte("\n"))
			}
		}

	}
	return nil
}
