package fmtstruct_test

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/efronlicht/fmtstruct"
)

func ExampleFormatModeFlag() {
	fs := flag.NewFlagSet("example-flags", flag.PanicOnError)
	_ = fmtstruct.FormatModeFlag(fs, "format", "output mode", fmtstruct.TABLE)
	*os.Stderr = *os.Stdout
	fs.Usage()
	// Output:
	// Usage of example-flags:
	//   -format value
	//     	output mode: one of [json json-pretty table csv]
}

func ExampleOPFormatVerbs() {
	type Student struct {
		FirstName   string `json:"first_name"`
		MiddleName  string `json:"middle_name,omitempty"`
		LastName    string `json:"last_name"`
		Age         int    `json:"age"`
		Permissions int    `json:"permissions,omitempty"` // like UNIX permissions
	}
	const R, W, X = 4, 2, 1
	students := []Student{
		{"Alice", "", "Smith", 20, R | W},
		{"Bob", "J", "Jones", 21, R | X},
		{"Charlie", "K", "Brown", 22, R | W | X},
		{"Snoopy", "T.", "Dog", 2, 0},
	}
	opts := fmtstruct.TABLE.DefaultOptions()
	opts.FormatVerbs = map[string]string{
		"Age":         "%02d",
		"Permissions": "0o%o",
	}
	if err := fmtstruct.WriteTo(os.Stdout, fmtstruct.TABLE, opts, students...); err != nil {
		panic(err)
	}
	// Output:
	//first_name  middle_name  last_name  age   permissions
	//----        ----         ----       ----  ----
	//Alice                    Smith      20    0o6
	//Bob         J            Jones      21    0o5
	//Charlie     K            Brown      22    0o7
	//Snoopy      T.           Dog        02
}

func ExampleWriteTo() {
	type S struct {
		PrintMe int
		OmitMe  int
	}
	opts := &fmtstruct.Opts{
		NoHeader:       false,
		PrintSeparator: true,
		Fields:         []string{"PrintMe"},
	}
	if err := fmtstruct.WriteTo(os.Stdout, fmtstruct.TABLE, opts, S{1, 2}, S{3, 4}, S{5, 6}); err != nil {
		panic(err)
	}
	// Output:
	// PrintMe
	// ----
	// 1
	// 3
	// 5
}

func ExamplePrintTable() {
	type S struct {
		A int `json:"a"`
		B int `json:"b"`
	}
	if err := fmtstruct.PrintTable(S{1, 2}, S{3, 4}, S{5, 6}); err != nil {
		panic(err)
	}

	// Output:
	//a     b
	//----  ----
	//1     2
	//3     4
	//5     6
}

func ExampleRegisterCLI() {
	fs := flag.NewFlagSet("example-flags", flag.PanicOnError)
	mode, opts := fmtstruct.RegisterCLI(fs, fmtstruct.TABLE)
	fs.Parse([]string{"-fmt-omitempty=false"})
	if *mode != fmtstruct.TABLE {
		panic("expected MODE_TABLE")
	}
	if opts == nil {
		panic("expected non-nil")
	}
	fmt.Printf(`mode=%s
opts.AllowOmitEmpty=%v
opts.NoHeader=%v
opts.PrintSeparator=%v
opts.Fields=%v
`, *mode, opts.AllowOmitEmpty, opts.NoHeader, opts.PrintSeparator, opts.Fields)
	// Output:
	// mode=table
	// opts.AllowOmitEmpty=false
	// opts.NoHeader=false
	// opts.PrintSeparator=true
	// opts.Fields=[]
}
