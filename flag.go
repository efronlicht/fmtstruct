package fmtstruct

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"strings"
)

// RegisterCLI registers the flags on a flag.FlagSet for setting the output mode and options.
// You can just use this on flag.CommandLine if you don't have a custom flag set. See the example.
func RegisterCLI(fs *flag.FlagSet, defaultMode Mode) (*Mode, *Opts) {
	mode := FormatModeFlag(fs, "format", "output mode", TABLE)
	opts := defaultMode.DefaultOptions()
	FormatOptFlag(fs, "fmt", opts)
	return mode, opts
}

// FormatModeFlag creates a flag for setting the output mode.
// Usually you just want to use RegisterCLI instead of this function.
func FormatModeFlag(fs *flag.FlagSet, name string, usage string, defaultValue Mode) *Mode {
	m := &defaultValue
	usage = fmt.Sprintf("%s: one of %v", usage, modeNames[1:])
	fs.Func(name, usage, func(s string) (err error) { *m, err = ParseMode(s); return err })
	return m
}

// FormatOptFlag creates flags for setting the options for formatting a struct.
// Usually, you just want to use [RegisterCLI] instead of this function.
// The prefix is used to create the flag names. For example, if prefix is "fmt",
// the flags will be "fmt-omitempty", "fmt-no-header", etc.
// Trailing '-' characters are removed from the prefix.
// The flags are:
//   - prefix-omitempty: allow omitting empty fields with the 'omitempty' JSON tag
//   - prefix-no-header: do not print the header row
//   - prefix-separator: print a separator line '----' between the header and the row
//   - prefix-verbs: map of field names to format verbs
//   - prefix-fields: which fields to print: comma-separated list of field names
//
// # Usage:
//
//	fs := flag.NewFlagSet("example-flags", flag.PanicOnError)
//	opts := new(fmtstruct.Opts)
//	opts.AddFlagSet(fs, "fmt")
func FormatOptFlag(fs *flag.FlagSet, prefix string, o *Opts) {
	prefix = strings.TrimSuffix(prefix, "-")
	fs.BoolVar(&o.AllowOmitEmpty, prefix+"-omitempty", o.AllowOmitEmpty, "allow omitting empty fields with the 'omitempty' JSON tag")
	fs.BoolVar(&o.NoHeader, prefix+"-no-header", o.NoHeader, "do not print the header row")
	fs.BoolVar(&o.PrintSeparator, prefix+"-separator", o.PrintSeparator, "print a separator line '----' between the header and the row")
	o.FormatVerbs = make(map[string]string)
	fs.Func(prefix+"-verbs", "map of field names to format verbs", func(s string) error {
		if err := json.Unmarshal([]byte(s), &o.FormatVerbs); err != nil {
			return fmt.Errorf("invalid format verbs: %w", err)
		}
		for _, v := range o.FormatVerbs {
			if strings.Count(v, "%") != 1 {
				return fmt.Errorf("invalid format verb %q: expected exactly one %%", v)
			}
		}
		return nil
	})
	fs.Func(prefix+"-fields", "which fields to print: comma-separated list of field names", func(s string) error {
		fields, err := csv.NewReader(strings.NewReader(s)).Read()
		if err != nil {
			return fmt.Errorf("invalid fields: %w", err)
		}
		o.Fields = fields
		return nil
	})
}
