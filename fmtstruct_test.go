package fmtstruct_test

import (
	"encoding/csv"
	"io"
	"strings"
	"testing"

	"gitlab.com/efronlicht/fmtstruct"
)

func TestAllOutputModes(t *testing.T) {
	for m := fmtstruct.Mode(1); m < fmtstruct.FORMAT_MODE_N; m++ {
		type S struct {
			A int `json:"a"`
			B int `json:"b,omitempty"`
		}

		t.Run(m.String(), func(t *testing.T) {
			var b strings.Builder
			if err := fmtstruct.WriteTo(&b, m, m.DefaultOptions(), S{1, 2}, S{3, 4}, S{5, 0}); err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestWriteCSV(t *testing.T) {
	type S struct {
		A int `json:"a"`
		B int `json:"b,omitempty"`
	}

	t.Run("bad format verb", func(t *testing.T) {
		if err := fmtstruct.WriteTo(io.Discard, fmtstruct.CSV, &fmtstruct.Opts{
			FormatVerbs: map[string]string{"A": "%%%v"},
		}, S{1, 2}, S{3, 4}, S{5, 0}); err == nil {
			t.Error("expected error")
		}
	})
	t.Run("omit fields", func(t *testing.T) {
		var b strings.Builder
		fmtstruct.WriteTo(&b, fmtstruct.CSV, &fmtstruct.Opts{
			Fields: []string{"A"},
		}, S{1, 2}, S{3, 4}, S{5, 0})
		got := b.String()
		if strings.Contains(got, "b") {
			t.Error("expected to not find b")
		}
	})
	t.Run("happy path", func(t *testing.T) {
		var b strings.Builder
		if err := fmtstruct.WriteTo(&b, fmtstruct.CSV, &fmtstruct.Opts{
			FormatVerbs:    map[string]string{"B": "%02d"},
			PrintSeparator: true,
			AllowOmitEmpty: false,
			NoHeader:       false,
		}, S{1, 2}, S{3, 4}, S{5, 6}); err != nil {
			t.Fatal(err)
		}

		got := b.String()
		t.Log("got", got)

		cr := csv.NewReader(strings.NewReader(got))
		cr.TrimLeadingSpace = true
		records, err := cr.ReadAll()
		if err != nil {
			t.Fatal(err)
		}
		t.Log("records", records)
		for i, want := range [][2]string{
			{"a", "b"},
			{"----", "----"},
			{"1", "02"},
			{"3", "04"},
			{"5", "06"},
		} {
			got := records[i]
			if len(got) != len(want) || got[0] != want[0] || got[1] != want[1] {
				t.Errorf("expected %v, got %v", want, got)
			}

		}
	})
}

func TestTableOmitEmpty(t *testing.T) {
	type S struct {
		A bool `json:"a"`
		B int  `json:"b,omitempty"`
	}
	var b strings.Builder
	fmtstruct.WriteTable(&b, &fmtstruct.Opts{AllowOmitEmpty: true}, []S{{true, 1}, {true, 0}, {false, 2}, {false, 3}}...)
	got := b.String()
	t.Log(got)
	for _, n := range []string{"1", "2", "3"} {
		if !strings.Contains(got, n) {
			t.Errorf("expected to find %q", n)
		}
	}
	for _, n := range []string{"0"} {
		if strings.Contains(got, n) {
			t.Errorf("expected to not find %q", n)
		}
	}
}

func TestTableUserSpecifiedFields(t *testing.T) {
	type S struct {
		A int `json:"a"`
		B int `json:"b_key,omitempty"`
	}
	var b strings.Builder
	if err := fmtstruct.WriteTable(&b, &fmtstruct.Opts{
		Fields: []string{"B"},
	},
		S{1, 2},
		S{3, 4},
		S{5, 0},
	); err != nil {
		t.Fatal(err)
	}
	t.Log(b.String())
	got := b.String()
	if !strings.Contains(got, "2") {
		t.Error("expected to find 2")
	}
	if strings.Contains(got, "1") {
		t.Error("expected to not find 1")
	}
	if !strings.Contains(got, "b_key") {
		t.Error("expected to find b_key")
	}
}

func TestTableFormatVerb(t *testing.T) {
	type S struct {
		A int `json:"a"`
		B int `json:"b,omitempty"`
	}
	var b strings.Builder
	fmtstruct.WriteTable(&b, &fmtstruct.Opts{FormatVerbs: map[string]string{"A": "%#02x", "B": "%06d"}},
		S{1, 2},
		S{3, 4},
		S{5, 0},
	)
	got := b.String()
	if !strings.Contains(got, "0x01") {
		t.Error("expected to find 0x01")
	}
}

func TestFmtstructSeparator(t *testing.T) {
	type S struct {
		Foo         string
		Bar         string `json:"bar,omitempty"`
		notExported int
	}
	var b strings.Builder
	fmtstruct.WriteTable(&b, &fmtstruct.Opts{PrintSeparator: true},
		S{Foo: "hello", Bar: "world", notExported: 42},
		S{Foo: "goodbye", Bar: "cruel-world", notExported: 42},
	)
	got := b.String()
	lines := strings.Split(got, "\n")
	expectedFields := [...][2]string{
		{"Foo", "bar"},
		{"----", "----"},
		{"hello", "world"},
		{"goodbye", "cruel-world"},
	}
	for i, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		gotFields := strings.Fields(line)
		if len(gotFields) != 2 {
			t.Fatalf("expected 2 fields, got %v", gotFields)
		}
		if gotFields[0] != expectedFields[i][0] || gotFields[1] != expectedFields[i][1] {
			t.Errorf("line #zs%d: expected %q, got %q", i, expectedFields[i], gotFields[:])
		}

	}
}
