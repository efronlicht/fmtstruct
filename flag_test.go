package fmtstruct_test

import (
	"flag"
	"testing"

	"gitlab.com/efronlicht/fmtstruct"
)

func TestRegisterCLI(t *testing.T) {
	fs := flag.NewFlagSet("test", flag.PanicOnError)
	mode, opts := fmtstruct.RegisterCLI(fs, fmtstruct.TABLE)
	fs.Parse(nil)
	if *mode != fmtstruct.TABLE {
		t.Errorf("expected MODE_TABLE, got %v", *mode)
	}
	if opts == nil {
		t.Fatalf("expected non-nil")
	}
}

func TestOptFlag(t *testing.T) {
	fs := flag.NewFlagSet("test", flag.PanicOnError)
	o := new(fmtstruct.Opts)
	fmtstruct.FormatOptFlag(fs, "opt", o)

	fs.Parse([]string{
		"-opt-separator=false",
		"-opt-omitempty=true",
		"-opt-no-header=true",
		"-opt-fields=a,b,c",
		`-opt-verbs={"a":"%d"}`,
	})
	if o.PrintSeparator {
		t.Error("expected PrintSeparator to be true")
	}
	if !o.AllowOmitEmpty {
		t.Error("expected AllowOmitEmpty to be true")
	}
	if !o.NoHeader {
		t.Error("expected NoHeader to be true")
	}
	if len(o.Fields) != 3 || o.Fields[0] != "a" || o.Fields[1] != "b" || o.Fields[2] != "c" {
		t.Errorf("expected [a b c], got %v", o.Fields)
	}
	if len(o.FormatVerbs) != 1 || o.FormatVerbs["a"] != "%d" {
		t.Errorf("expected {a: %%d}, got %v", o.FormatVerbs)
	}
}

func TestModeFlag(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		for want := fmtstruct.Mode(1); want < fmtstruct.FORMAT_MODE_N; want++ {
			fs := flag.NewFlagSet("test", flag.PanicOnError)
			m := fmtstruct.FormatModeFlag(fs, "mode", "output mode", fmtstruct.TABLE)
			fs.Parse([]string{"-mode", want.String()})
			if *m != want {
				t.Errorf("expected %v, got %v", want, *m)
			}
		}
	})
	t.Run("bad mode", func(t *testing.T) {
		fs := flag.NewFlagSet("test", flag.PanicOnError)
		m := fmtstruct.FormatModeFlag(fs, "mode", "output mode", fmtstruct.TABLE)
		defer func() {
			if recover() == nil {
				t.Error("expected panic")
			}
		}()
		fs.Parse([]string{"-mode", "badmode"})
		_ = m
	})
}
